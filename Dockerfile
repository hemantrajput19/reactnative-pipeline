FROM ubuntu:18.04

RUN mkdir android-sdk
RUN mkdir android-sdk/cmdline-tools
RUN apt update
RUN apt install openjdk-8-jdk -y
RUN apt-get install wget -y
RUN wget https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip
RUN apt install unzip
RUN unzip *.zip -d /android-sdk/cmdline-tools 
RUN rm *.zip
ENV ANDROID_HOME="/android-sdk"
ENV PATH="$PATH:$ANDROID_HOME/cmdline-tools/tools/bin"
RUN yes | sdkmanager "platform-tools" "platforms;android-28" "build-tools;28.0.3"
ENV PATH="$PATH:$ANDROID_HOME/platform-tools"
ENV export ANDROID_SDK_ROOT="/android-sdk/"
RUN apt-get update
RUN apt-get install curl -y
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash 
RUN apt-get install -y nodejs
RUN node -v
RUN mkdir project
WORKDIR /project
CMD [ "ls" ]
